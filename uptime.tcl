### Uptime contest
#
# Small script to do uptime contests
# All people in the channel (unless they are in excluded list)
# will be voiced and added to the list of participants
# each time someone quits the channel (quit, part, kick), (s)he loses

## Configuration
# - set triggerS (start trigger) and triggerE (end trigger)
#    to fit your need
# - set excluded which is the list of excluded nicks
#    note that $::botnick is not mandatory (internal checks exist)
#    also note to protect special chars
# - set tformat (datetime format) and precision

## Running
# - add +upcontest flag to your chan(s)
# - start with !start

## Additionnal command
# !setprize allow to add a prize for the winner
# => "!setprize great surprise" will add a great surprise
# => "!setprize" will remove prize

namespace eval uptime {

   variable triggerS "!start"
   variable triggerE "!stop"
   
   # list of excluded nick
   variable excluded {$::botnick "Q" "ChanServ" "\[Bot1\]"}
   
   # Format of the date (see strftime documentation)
   variable tformat "%d/%m/%Y %H:%M:%S"
   
   # precision of the duration
   variable precision 3
   
   variable counter 0
   
   setudef flag upcontest
   
   variable players
   bind pub -|n $::uptime::triggerS ::uptime::start
   bind pub -|n $::uptime::triggerE ::uptime::stop
   bind pub -|n !setprize ::uptime::setprize
   
   variable tstart
   
   proc start {nick uhost handle chan text} {
      if {![channel get $chan upcontest]} { return }
      if {[info exists ::uptime::players($chan)] && [llength ::uptime::players($chan)]>0} { return }
      set ::uptime::players($chan) {}
      foreach u [chanlist $chan] {
         if {[isbotnick $u] || [lsearch -nocase $::uptime::excluded $u]>-1} { continue }
         pushmode $chan +v $u
         lappend ::uptime::players($chan) $u
      }
      flushmode $chan
      set ::uptime::tstart [clock seconds]
	  incr ::uptime::counter
	  if {![info exists ::uptime::prize] || $::uptime::prize eq ""} {
	     set prize ""
	  } else {
	     set prize " - Prize: $::uptime::prize"
	  }
      putserv "TOPIC $chan :New Uptime contest started at [clock format $::uptime::tstart -format $::uptime::tformat]$prize"
      putserv "PRIVMSG $chan :Uptime contest is now running!"
   }
   
   proc stop {nick uhost handle chan text} {
      if {![info exists ::uptime::players($chan)] || [llength ::uptime::players($chan)]==0} { return }
      foreach u $::uptime::players($chan) {
         pushmode $chan -v $u
      }
      flushmode $chan
      unset ::uptime::players($chan)
      unset ::uptime::tstart
      putserv "PRIVMSG $chan :Uptime contest stopped by $nick"
      putserv "TOPIC $chan :Uptime contest stopped by $nick at [clock format [clock seconds] -format $::uptime::tformat]"
   }
   
   proc setprize {nick uhost handle chan text} {
      if {[string trim $text] eq "" } {
         unset ::uptime::prize
		} else {
			set ::uptime::prize $text
		}
	}
   
   bind nick - * ::uptime::renuser
   proc renuser {nick uhost handle chan nnick} {
      if {![info exists ::uptime::players($chan)]} { return }
      set ind [lsearch $::uptime::players($chan) $nick]
      if { $ind > -1} {
         set ::uptime::players($chan) [lreplace $::uptime::players($chan) $ind $ind $nnick]
      }
   }
   
   bind part - * ::uptime::part
   proc part {nick uhost handle chan text} {
      if {![info exists ::uptime::players($chan)]} { return }
      ::uptime::lose $chan $nick
   }
   
   bind sign - * ::uptime::sign
   proc sign {nick uhost handle chan text} {
      if {![info exists ::uptime::players($chan)]} { return }
      ::uptime::lose $chan $nick
   }
   
   bind kick - * ::uptime::kick
   proc kick {nick uhost handle chan target reason} {
      if {![info exists ::uptime::players($chan)]} { return }
      ::uptime::lose $chan $target
   }
   
   proc lose {chan nick} {
      if {![info exists ::uptime::players($chan)]} { return }
      set ind [lsearch $::uptime::players($chan) $nick]
      if {$ind > -1} {
         putserv "PRIVMSG $chan :$nick is no more playing ! LoOose !"
         set ::uptime::players($chan) [lreplace $::uptime::players($chan) $ind $ind]
      }
      if {[llength $::uptime::players($chan)]==1} {
         putserv "PRIVMSG $chan :And the winner is... [lindex $::uptime::players($chan) 0]"
         putserv "TOPIC $chan :Uptime contest finished ([clock format $::uptime::tstart -format $::uptime::tformat] - [clock format [clock seconds] -format $::uptime::tformat]) - Uptime winner is [lindex $::uptime::players($chan) 0] with [::uptime::duration $::uptime::tstart] - Total contests: $::uptime::counter"
         unset ::uptime::players($chan)
      }
   }
   
   proc duration {start {end 0}} {
      set slang {year month day hour minute second}
      if {$end eq 0} {
         set end [clock seconds]
      }
      set out {}
      set years [expr {[clock format $end -format %Y]  - [clock format $start -format %Y]}]
      set delay [clock format [expr {$end - $start}] -format "%m-%d %H:%M:%S"]
      regexp {(\d+)-(\d+) 0?(\d+):0?(\d+):0?(\d+)} $delay -> months days hours minutes seconds
      set tdata [list $years [incr months -1] [incr days -1] [incr hours -1] $minutes $seconds]
      set i 0
      foreach val $tdata {
         if {$val > 0} {
            if {$val>1} { set s "s" } else { set s "" }
            lappend out "$val [lindex $slang $i]$s"
         }
         incr i
      }
      if {$::uptime::precision <= 2 || [llength $out]<$::uptime::precision} {
         set tmpret [join [lrange $out 0 [expr {$::uptime::precision - 1}]] " and "]
      } else {
         set tmpret [join [lrange $out 0 [expr {$::uptime::precision - 2}]] ", "]
         set tmpret "$tmpret and [join [lindex $out [expr {$::uptime::precision - 1}]]]"
      }
      return $tmpret
   }

   putlog "Uptime Contest v220522 by CrazyCat <https://forum.eggdrop.fr> Loaded"
}
